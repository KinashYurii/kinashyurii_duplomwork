import numpy
import theano
import os

os.environ['KERAS_BACKEND'] = 'theano'
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils

numpy.random.seed(42)

img_rows, img_cols = 28, 28

(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)
input_shape = (1, img_rows, img_cols)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

model = Sequential()

model.add(Conv2D(75, (5, 5),
                 'relu',
                 input_shape))
model.add(MaxPooling2D((2, 2)))
model.add(Dropout(0.2))
model.add(Conv2D(100, (5, 5), 'relu'))
model.add(MaxPooling2D((2, 2)))
model.add(Dropout(0.2))
model.add(Flatten())
model.add(Dense(500, 'relu'))
model.add(Dropout(0.5))
model.add(Dense(10, 'softmax'))

model.compile("categorical_crossentropy", "adam", ["accuracy"])

print(model.summary())

model.fit(X_train, Y_train, 200, 10, 0.2, 2)

scores = model.evaluate(X_test, Y_test, 0)
print("Точность работы на тестовых данных: %.2f%%" % (scores[1] * 100))
