﻿/* WordprocessingDocument wd = WordprocessingDocument.Create("C:\\Users\\Yura\\Desktop\\FirsOrders.docx", DocumentFormat.OpenXml.WordprocessingDocumentType.Document);
            Word._Application MyApp = new Word.Application();
            MyApp.Visible = true;

            Word.Document document = MyApp.Documents.Add();


            object missing = System.Reflection.Missing.Value;
            for (int i = 0; i < 10; i++)
            {
                document.Paragraphs.Add(ref missing);
                Word.Range wRng = document.Paragraphs.Last.Range;

                wRng.Text = "dy/dx + y'_x = x";

                var mathRange = wRng.OMaths.Add(wRng);
                mathRange.OMaths[1].BuildUp();
            }

    */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using Word = Microsoft.Office.Interop.Word;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Reflection;
using System.Drawing.Imaging;

namespace ConsoleApp2
{
    
    class Program
    {
        static void CreateFormula(string str1, string str2, string filename, int size, int loopint, int filenameint, string[] font)
        {
            System.Drawing.Font myFont = new System.Drawing.Font(font[loopint], size, FontStyle.Italic);
            g.Flush();
            g.Clear(System.Drawing.Color.White);
            Drow(str1, str2, myFont);
            Save(filename, filenameint);
        }
        static void Drow(string str1, string str2, System.Drawing.Font myFont)
        {
            g.DrawString(str1, myFont, new SolidBrush(System.Drawing.Color.Black), rect);
            g.DrawString(str2, myFont, new SolidBrush(System.Drawing.Color.Black), rect);
        }
        static void Save(string pathSave, int i)
        {
            var currentDirectory = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + pathSave;
            bool exists = System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + pathSave);
            if (!exists)
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + pathSave);
            string fileName = currentDirectory + i.ToString() + ".png";
            bmp.Save(fileName, ImageFormat.Png);
        }
        static string[] Y = new string[8];
        static string[] Y_diff = new string[5];
        static string[] F_x = new string[30];
        static string[] F_y = new string[30];
        static string[] operation = new string[2];
        static Graphics g;
        static System.Drawing.Image bmp;
        static Rectangle rect;
        const int trainSize = 15000;
        const int testSize = 5000;
        const int validationSize = 5000;
        static void Main(string[] args)
        {
           
            Random random = new Random();
            string[] font = { "Arial", "Lucida Console", "Times New Roman", "Verdana",
                "Comic Sans MS","Impact","Courier New"};
            bmp = new Bitmap(200, 40);
            g = Graphics.FromImage(bmp);
            int size=5;
            rect = new Rectangle(0, 0, 200, 40);
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            
            F_x[0] = "x"; F_x[1] = "exp{x}"; F_x[2] = "ln(x)"; F_x[3] = "sin(x)"; F_x[4] = "x+10";
            F_x[5] = "3x"; F_x[6] = "(x+sin(x))"; F_x[7] = "x"; F_x[8] = "x(x+1)"; F_x[9] = "(x-10)";
            F_x[10] = "cos(x)"; F_x[11] = "tg(x)"; F_x[12] = "xctg(x)"; F_x[13] = "(x-sin(x)+ln(x))"; F_x[14] = "";
            F_x[15] = "xcos(x)"; F_x[16] = "xln(x)"; F_x[17] = "xsin(x)"; F_x[18] = "(x+10x)"; F_x[19] = "31x";
            F_x[20] = "xsin(x)cos(x)"; F_x[21] = "sin(x)ln(x)"; F_x[22] = "lg(x)"; F_x[23] = "(x+10lg(x))"; F_x[24] = "10";
            F_x[25] = "xtg(x)cos(x)"; F_x[26] = "cos(x)ln(x)"; F_x[27] = "xlg(x)"; F_x[28] = "(six(x)+10x)"; F_x[29] = "cos(x)sin(x)";

            F_y[0] = "y"; F_y[1] = "exp{y}"; F_y[2] = "ln(y)"; F_y[3] = "sin(y)"; F_y[4] = "y+10";
            F_y[5] = "3y"; F_y[6] = "(y+sin(y))"; F_y[7] = ""; F_y[8] = "y(y+1)"; F_y[9] = "(y-10)";
            F_y[10] = "cos(y)"; F_y[11] = "tg(y)"; F_y[12] = "yctg(y)"; F_y[13] = "(y-sin(y)+ln(y))"; F_y[14] = "";
            F_y[15] = "ycos(y)"; F_y[16] = "yln(y)"; F_y[17] = "ysin(y)"; F_y[18] = "(y+10y)"; F_y[19] = "31y";
            F_y[20] = "ysin(y)cos(y)"; F_y[21] = "sin(y)ln(y)"; F_y[22] = "lg(y)"; F_y[23] = "(y+10lg(y))"; F_y[24] = "10";
            F_y[25] = "ytg(y)cos(y)"; F_y[26] = "cos(y)ln(y)"; F_y[27] = "ylg(y)"; F_y[28] = "(six(y)+10y)"; F_y[29] = "cos(y)sin(y)";

            operation[0] = "+"; operation[1] = "-";

            Y[0] = "y²"; Y[1] = "y³"; Y[2] = "y⁴"; Y[3] = "y⁵";
            Y[4] = "y⁶"; Y[5] = "y⁷"; Y[6] = "y⁸"; Y[7] = "y⁹";

            Y_diff[0] = "y'"; Y_diff[1] = "y''"; Y_diff[2] = "y'''"; Y_diff[3] = "y''''"; Y_diff[4] = "y'''''";

            
            CreateVidocremzZmin(random, size, font);
            CreateBernulli(random, size, font);
            CreatePonujenyaStep(random, size, font);


        }
      static void CreateVidocremzZmin(Random random,int size,string[] font) {
            Console.WriteLine("Train");
            VidokremluvaniZminniTrain(random, size, font);
            Console.WriteLine("Validation");
            VidokremluvaniZminniValidation(random, size, font);
            Console.WriteLine("Test");
            VidokremluvaniZminniTest(random, size, font);
        }
      static void VidokremluvaniZminniTrain(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/train/VidocremZminni/";
            for (int i = 0; i < trainSize; i++) { 
                j++;
                string str1 = "";
                string str2 = "";
                switch (random.Next(8))
                {
                    case 0:
                        str1 = "dy" + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length)] + " "  + operation[random.Next(1)] + " "
                   + "dx" + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length)] + " = 0";
                        str2 = "";
                        break;
                    case 1:
                        str1 = "dx" + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)]  + " " + operation[random.Next(1)] + " "
                   + "dy" + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + " = 0";
                        str2 = "";
                        break;
                    case 2:
                        str1 = F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + "dy"  + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " = 0";
                        str2 = "";
                        break;
                    case 3:
                        str1 = F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " "  + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " = 0";
                        str2 = "";
                        break;
                    case 4:
                        str1 = "y'" + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + " = 0";
                        str2 = "";
                        break;
                    case 5:
                        str1 = F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " = "
                            + "dy" + F_y[random.Next(19)] + F_x[random.Next(F_x.Length - 1)];
                        str2 = "";
                        break;
                    case 6:
                        str1 = F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " = "
                            + "dx" + F_y[random.Next(19)] + F_x[random.Next(F_x.Length - 1)];
                        str2 = "";
                        break;
                    case 7:
                        str1 = "dx"+F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + " = "  
                            + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)] + "dy";
                        str2 = "";
                        break;
                    case 8:
                        str1 = "dy" + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length - 1)]  + " = "  
                            + F_y[random.Next(F_y.Length-1)] + F_x[random.Next(F_x.Length-1)] + "dx";
                        str2 = "";
                        break;

                }
                CreateFormula(str1, str2, pathName, size + random.Next(7),random.Next(7),j,font);
            }
        }
      static void VidokremluvaniZminniValidation(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/validation/VidocremZminni/";
            for (int i = 0; i < validationSize; i++)
            {
                j++;
                string str1 = "";
                string str2 = "";
                switch (random.Next(8))
                {
                    case 0:
                        str1 = "dy" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length)] + " " + operation[random.Next(1)] + " "
                   + "dx" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length)] + " = 0";
                        str2 = "";
                        break;
                    case 1:
                        str1 = "dx" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " " + operation[random.Next(1)] + " "
                   + "dy" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = 0";
                        str2 = "";
                        break;
                    case 2:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " = 0";
                        str2 = "";
                        break;
                    case 3:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " = 0";
                        str2 = "";
                        break;
                    case 4:
                        str1 = "y'" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = 0";
                        str2 = "";
                        break;
                    case 5:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " = "
                            + "dy" + F_y[random.Next(19)] + F_x[random.Next(F_x.Length - 1)];
                        str2 = "";
                        break;
                    case 6:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " = "
                            + "dx" + F_y[random.Next(19)] + F_x[random.Next(F_x.Length - 1)];
                        str2 = "";
                        break;
                    case 7:
                        str1 = "dx" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = "
                            + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy";
                        str2 = "";
                        break;
                    case 8:
                        str1 = "dy" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = "
                            + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx";
                        str2 = "";
                        break;

                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }
        }
      static void VidokremluvaniZminniTest(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/test/VidocremZminni/";
            for (int i = 0; i < testSize; i++)
            {
                j++;
                string str1 = "";
                string str2 = "";
                switch (random.Next(8))
                {
                    case 0:
                        str1 = "dy" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length)] + " " + operation[random.Next(1)] + " "
                   + "dx" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length)] + " = 0";
                        str2 = "";
                        break;
                    case 1:
                        str1 = "dx" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " " + operation[random.Next(1)] + " "
                   + "dy" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = 0";
                        str2 = "";
                        break;
                    case 2:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " = 0";
                        str2 = "";
                        break;
                    case 3:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " = 0";
                        str2 = "";
                        break;
                    case 4:
                        str1 = "y'" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " " + operation[random.Next(1)] + " "
                    + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = 0";
                        str2 = "";
                        break;
                    case 5:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx" + " = "
                            + "dy" + F_y[random.Next(19)] + F_x[random.Next(F_x.Length - 1)];
                        str2 = "";
                        break;
                    case 6:
                        str1 = F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy" + " = "
                            + "dx" + F_y[random.Next(19)] + F_x[random.Next(F_x.Length - 1)];
                        str2 = "";
                        break;
                    case 7:
                        str1 = "dx" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = "
                            + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dy";
                        str2 = "";
                        break;
                    case 8:
                        str1 = "dy" + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + " = "
                            + F_y[random.Next(F_y.Length - 1)] + F_x[random.Next(F_x.Length - 1)] + "dx";
                        str2 = "";
                        break;

                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }
        }

        static void CreateBernulli(Random random, int size, string[] font)
        {
            Console.WriteLine("Train");
            BernulliTrain(random, size, font);
            Console.WriteLine("Validation");
            BernulliValidation(random, size, font);
            Console.WriteLine("Test");
            BernulliTest(random, size, font);
        }
        static void BernulliTrain(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/train/Bernulli/";
            string str1="";
            string str2="";
            for (int i = 0; i < trainSize; i++)
            {
                j++;
                switch (random.Next(8))
                {
                    case 0:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " "+ operation[random.Next(operation.Length)] + " "+
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 "= 0";
                        str2 = "";
                        break;
                    case 1:
                        str1 =  "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 "= 0";
                        str2 = "";
                        break;
                    case 2:
                        str1 =   F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 " = " + F_x[random.Next(F_x.Length)] + "y'";
                        str2 = "";
                        break;
                    case 3:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + 
                                 "= " + F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)];
                        str2 = "";
                        break;
                    case 4:
                        str1 =   F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 " = " + F_x[random.Next(F_x.Length)] + "y";
                        str2 = "";
                        break;
                    case 5:
                        str1 =   F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +" " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y'" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 6:
                        str1 = F_x[random.Next(F_x.Length)] + "y"  + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 7:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 8:
                        str1 = Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 9:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                "y" +
                                 "= 0";
                        str2 = "";
                        break;
                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }
                          
            }

        static void BernulliTest(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/test/Bernulli/";
            string str1 = "";
            string str2 = "";
            for (int i = 0; i < testSize; i++)
            {
                j++;
                switch (random.Next(8))
                {
                    case 0:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 "= 0";
                        str2 = "";
                        break;
                    case 1:
                        str1 = "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 "= 0";
                        str2 = "";
                        break;
                    case 2:
                        str1 = F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 " = " + F_x[random.Next(F_x.Length)] + "y'";
                        str2 = "";
                        break;
                    case 3:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " +
                                 "= " + F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)];
                        str2 = "";
                        break;
                    case 4:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 " = " + F_x[random.Next(F_x.Length)] + "y";
                        str2 = "";
                        break;
                    case 5:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y'" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 6:
                        str1 = F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 7:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 8:
                        str1 = Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 9:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                "y" +
                                 "= 0";
                        str2 = "";
                        break;
                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }

        }

        static void BernulliValidation(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/validation/Bernulli/";
            string str1 = "";
            string str2 = "";
            for (int i = 0; i < validationSize; i++)
            {
                j++;
                switch (random.Next(8))
                {
                    case 0:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 "= 0";
                        str2 = "";
                        break;
                    case 1:
                        str1 = "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 "= 0";
                        str2 = "";
                        break;
                    case 2:
                        str1 = F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 " = " + F_x[random.Next(F_x.Length)] + "y'";
                        str2 = "";
                        break;
                    case 3:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " +
                                 "= " + F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)];
                        str2 = "";
                        break;
                    case 4:
                        str1 = F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] +
                                 " = " + F_x[random.Next(F_x.Length)] + "y";
                        str2 = "";
                        break;
                    case 5:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                                 F_x[random.Next(F_x.Length)] + "y'" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 6:
                        str1 = F_x[random.Next(F_x.Length)] + "y" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 7:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 8:
                        str1 = Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y" +
                                 "= 0";
                        str2 = "";
                        break;
                    case 9:
                        str1 = F_x[random.Next(F_x.Length)] + Y[random.Next(Y.Length)] + " " + operation[random.Next(operation.Length)] + " " +
                               F_x[random.Next(F_x.Length)] + "y'" + " " + operation[random.Next(operation.Length)] + " " +
                                "y" +
                                 "= 0";
                        str2 = "";
                        break;
                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }

        }

        static void CreatePonujenyaStep(Random random, int size, string[] font)
        {
            Console.WriteLine("Train");
            PonujenyaStepTrain(random, size, font);
            Console.WriteLine("Validation");
            PonujenyaStepValidation(random, size, font);
            Console.WriteLine("Test");
            PonujenyaStepTest(random, size, font);
        }
        static void PonujenyaStepTrain(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/train/PonujenyaStep/";
            string str1 = "";
            string str2 = "";
            for (int i = 0; i < trainSize / 3; i++) 
            {
                j++;
                switch (random.Next(1)) {
                    case 0:
                        str1 = Y_diff[random.Next(Y_diff.Length)] + " = " + F_x[random.Next(F_x.Length)];
                        break;
                    case 1:
                        str1 = F_x[random.Next(F_x.Length)] + " = " + Y_diff[random.Next(Y_diff.Length)];
                        break;

                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }

        }
        static void PonujenyaStepTest(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/test/PonujenyaStep/";
            string str1 = "";
            string str2 = "";
            for (int i = 0; i < testSize / 5; i++) 
            {
                j++;
                switch (random.Next(1))
                {
                    case 0:
                        str1 = Y_diff[random.Next(Y_diff.Length)] + " = " + F_x[random.Next(F_x.Length)];
                        break;
                    case 1:
                        str1 = F_x[random.Next(F_x.Length)] + " = " + Y_diff[random.Next(Y_diff.Length)];
                        break;

                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }

        }
        static void PonujenyaStepValidation(Random random, int size, String[] font)
        {
            int j = 0;
            string pathName = "/" + "first order" + "/Validation/PonujenyaStep/";
            string str1 = "";
            string str2 = "";
            for (int i = 0; i < validationSize / 5; i++) 
            {
                j++;
                switch (random.Next(1))
                {
                    case 0:
                        str1 = Y_diff[random.Next(Y_diff.Length)] + " = " + F_x[random.Next(F_x.Length)];
                        break;
                    case 1:
                        str1 = F_x[random.Next(F_x.Length)] + " = " + Y_diff[random.Next(Y_diff.Length)];
                        break;

                }
                CreateFormula(str1, str2, pathName, size + random.Next(7), random.Next(7), j, font);
            }

        }
    }
    }